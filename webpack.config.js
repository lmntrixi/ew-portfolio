const webpack = require('webpack');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
    mode: 'development',
    entry: [
        "./wp-content/themes/emma_portfolio/assets/js/index.js",
        "./wp-content/themes/emma_portfolio/assets/scss/style.scss"
    ],
    output: {
        path: __dirname,
        filename: "./wp-content/themes/emma_portfolio/assets/bundle/bundle.js"
    },
    devtool: 'eval-source-map',
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel-loader',
                query: {
                    presets: ['es2015']
                }
            },
            { // sass / scss loader for webpack
                test: /\.(sass|scss)$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    'postcss-loader',
                    'sass-loader'
                ]
            },
            {
                test: /\.svg/,
                use: {
                    loader: 'svg-url-loader',
                    options: {
                        name: './img/[name].[ext]',
                    }
                }
            }
        ]
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: "./wp-content/themes/emma_portfolio/assets/css/main.css",
        })
    ],
};