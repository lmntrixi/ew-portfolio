<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'ew-portfolio');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'Db`8~z9xwazeZ;2G|8G/]2/7[Sl$Ox,FXUe%o:idI8pks@R^RsP~C?H}ibUYd~1 ');
define('SECURE_AUTH_KEY', '48IQ}`-L(5iL9U*0J:Hmej/-j)V!`^9ooq-TEMxOKV|WT9]Fk&e$Kn(;_xlUwr27');
define('LOGGED_IN_KEY', '|]:fE3WQbINe-iO64gl6zri17xJ2x}q0ji:wB868AF.rLK~@n<:Mr$e|j<J9>^;_');
define('NONCE_KEY', 'JQMlm{XDqnL1 Ivj$+X+H]^ud x5g)YgiWe0XLv!E1}!y7nE9F%f`TUBuG}I;N}J');
define('AUTH_SALT', 'DOyPSkM;2pw2PFlwKI<M4Lg0Y0hvy;(JOI-SE)jz<>Z[tsBwsmje_S2@`ww3;&ZL');
define('SECURE_AUTH_SALT', '|%P5c#Y@nZ<09Eu6X.C2B8~$x8T:35rm7oXdo-B3rC8Be%5;Nb6zoZT]Y75.A^)6');
define('LOGGED_IN_SALT', 'CE*CfHQ{p/-x:Ql2Fa?e 92nf+|k|>S4{8R3z$LGUw`}L-KS<9T~3,&($&@xAISR');
define('NONCE_SALT', 'ljfet6HuCJu%fy/Lfhz89Oj%yxK@F:`l[_9vL+y}Xd&fO%Sz>2<MfZm/`n`Z 9=$');
/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if (!defined('ABSPATH'))
    define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
