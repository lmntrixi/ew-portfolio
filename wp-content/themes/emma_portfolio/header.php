<?php
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <?php wp_head(); ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
</head>
<body <?php body_class(); ?>>
<div id="page" class="site">

    <header id="masthead" class="site-header" role="banner">
        <div class="header-wrapper">
            <nav>
                <ul class='logo'>
                    <li><a href="index.php/home"><img src="http://emma.stijnolthof.nl/wp-content/themes/emma_portfolio/assets/images/LOGO-01.png" alt=""></a></li>
                </ul>
<!--                <div class="header-search"><div class="inner-wrapper"><i class="search-icon"></i><input type="search" title="search"><i class="closer class-toggle"></i></div></div>-->
                <ul class="main-nav">
                    <?php while (have_rows('nav', 'navigation')) : the_row(); ?>
                        <li class="text-1 tablet-remove"><a class='link' href="<?php the_sub_field('nav_url') ?>"><?php the_sub_field('nav_display_text', 'navigation'); ?></a></li>

                    <?php endwhile; ?>
<!--                    <li class="tablet-remove"><a href="#"><span class="icon-search component-search class-toggle"></span></a></li>-->
                </ul>
                <ul class="header-cta">
                    <a href="/index.php/contact"><li class="cta">CONTACT</li></a>
                    <li class="hamburger tablet-view" onclick="openNav()"><span class="icon-bars"></span></li>
                </ul>
            </nav>
        </div>



    </header><!-- #masthead -->


    <div id="myNav" class="overlay">
        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
        <div class="overlay-content">
            <?php while (have_rows('nav', 'navigation')) : the_row(); ?>
                <a class='link' href="<?php the_sub_field('nav_url') ?>"><?php the_sub_field('nav_display_text', 'navigation'); ?></a>

            <?php endwhile; ?>
<!--            <div class="searchbar">-->
<!--                <div class="inner-wrapper">-->
<!--                    <i class="search-icon"></i>-->
<!--                    <input id="re" title="search" type="search">-->
<!--                </div>-->
<!--            </div>-->
        </div>
    </div>


    <script>
        var hei=$( ".site-header" ).innerHeight(),
            f1 = $('.container .h1').offset().top,
            f2 = $('.container .h2').offset().top,
            f3 = $('.container .h3').offset().top,
            f4 = $('.container .h4').offset().top,
            myHeader = $( ".site-header" ),
            myWindow = $( window );
        function makeSticky() {
            $('.info-container').css({'padding-top':hei});
            myWindow.scroll( function() {
                if ( myWindow.scrollTop() == 0 ) {
                    myHeader.removeClass( "sticky-nav" );
                } else {
                    myHeader.addClass( "sticky-nav" );
                }
            } );
        }
        function remove_highlight()
        {
            $('nav ul li a').each(function(){
                $(this).removeClass('green');
            })
        }
        function jump()
        {
            $('nav ul li a').click(function(){
                event.preventDefault();
                var str = $(this).parents().index();
                if (str == 0) {
                    $('body,html').animate({scrollTop:f1 + hei});
                }
                if (str == 1) {
                    $('body,html').animate({scrollTop:f2 + hei});
                }
                if (str == 2) {
                    $('body,html').animate({scrollTop:f3 + hei});
                }
                if (str == 3) {
                    $('body,html').animate({scrollTop:f4 + hei});
                };
            })
        }
        $( function() {
            jump();
            makeSticky();
            $(window).scroll(function(){
                if ($(window).scrollTop() > f1 - hei) {
                    remove_highlight();
                    $('nav ul li:nth-child(1) a').addClass('green');

                }
                if ($(window).scrollTop() > f2 - hei) {
                    remove_highlight();
                    $('nav ul li:nth-child(2) a').addClass('green');

                }
                if ($(window).scrollTop() > f3 - hei) {
                    remove_highlight();
                    $('nav ul li:nth-child(3) a').addClass('green');

                }
                if ($(window).scrollTop() > f4 - hei) {
                    remove_highlight();
                    $('nav ul li:nth-child(4) a').addClass('green');

                };
            })

        } );

        function openNav() {
            document.getElementById("myNav").style.height = "100%";
        }

        function closeNav() {
            document.getElementById("myNav").style.height = "0%";
        }

    </script>


    <div class="site-content-contain">
        <div id="content" class="site-content">
