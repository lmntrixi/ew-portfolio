<?php
get_header(); ?>

    <div class="wrap">
        <div id="primary" class="content-area">
            <main id="main" class="site-main" role="main">
                <?php if (have_rows('content_block') == NULL): ?>

                    <div class="header-banner-wrapper">
                    <div class="header-banner">
                        <div class="darkened-background"></div>
                    </div>
                    <div class="title-wrapper">
                        <h1 class="title-4"><?php the_field("header_text") ?></h1>
                    </div>
                </div>
                <?php endif; ?>


                <?php
                get_home();
                get_tutorials();
                get_about();
                get_portfolio();
                get_contact();
                ?>
            </main>
        </div>
    </div>
<?php get_footer();
