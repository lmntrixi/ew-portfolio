<?php
if (have_rows('tutorial')) {
    ?>
    <section class="block-tutorials" style="padding-top: 0px">
        <div class="row">
            <?php
            while (have_rows('tutorial')) : the_row(); ?>
                <div class="tutorial">
                    <div class="video-wrapper">


                        <iframe src="<?php the_sub_field('tutorials_url')?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                    </div>
                    <h1><?php the_sub_field('tutorials_title') ?></h1>
                    <div class="info_tutorial">
                        <?php the_sub_field('tutorials_info') ?>
                    </div>
                </div>
            <?php
            endwhile;
            ?>
        </div>
    </section>

    <?php
} ?>

