/* global emma_portfolioScreenReaderText */
/**
 * Theme functions file.
 *
 * Contains handlers for navigation and widget area.
 */

(function( $ ) {
	var masthead, menuToggle, siteNavContain, siteNavigation;

	function initMainNavigation( container ) {

		// Add dropdown toggle that displays child menu items.
		var dropdownToggle = $( '<button />', { 'class': 'dropdown-toggle', 'aria-expanded': false })
			.append( emma_portfolioScreenReaderText.icon )
			.append( $( '<span />', { 'class': 'screen-reader-text', text: emma_portfolioScreenReaderText.expand }) );

		container.find( '.menu-item-has-children > a, .page_item_has_children > a' ).after( dropdownToggle );

		// Set the active submenu dropdown toggle button initial state.
		container.find( '.current-menu-ancestor > button' )
			.addClass( 'toggled-on' )
			.attr( 'aria-expanded', 'true' )
			.find( '.screen-reader-text' )
			.text( emma_portfolioScreenReaderText.collapse );
		// Set the active submenu initial state.
		container.find( '.current-menu-ancestor > .sub-menu' ).addClass( 'toggled-on' );

		container.find( '.dropdown-toggle' ).click( function( e ) {
			var _this = $( this ),
				screenReaderSpan = _this.find( '.screen-reader-text' );

			e.preventDefault();
			_this.toggleClass( 'toggled-on' );
			_this.next( '.children, .sub-menu' ).toggleClass( 'toggled-on' );

			_this.attr( 'aria-expanded', _this.attr( 'aria-expanded' ) === 'false' ? 'true' : 'false' );

			screenReaderSpan.text( screenReaderSpan.text() === emma_portfolioScreenReaderText.expand ? emma_portfolioScreenReaderText.collapse : emma_portfolioScreenReaderText.expand );
		});
	}

	initMainNavigation( $( '.main-navigation' ) );

	masthead       = $( '#masthead' );
	menuToggle     = masthead.find( '.menu-toggle' );
	siteNavContain = masthead.find( '.main-navigation' );
	siteNavigation = masthead.find( '.main-navigation > div > ul' );

	// Enable menuToggle.
	(function() {

		// Return early if menuToggle is missing.
		if ( ! menuToggle.length ) {
			return;
		}

		// Add an initial value for the attribute.
		menuToggle.attr( 'aria-expanded', 'false' );

		menuToggle.on( 'click.emma_portfolio', function() {
			siteNavContain.toggleClass( 'toggled-on' );

			$( this ).attr( 'aria-expanded', siteNavContain.hasClass( 'toggled-on' ) );
		});
	})();

	// Fix sub-menus for touch devices and better focus for hidden submenu items for accessibility.
	(function() {
		if ( ! siteNavigation.length || ! siteNavigation.children().length ) {
			return;
		}

		// Toggle `focus` class to allow submenu access on tablets.
		function toggleFocusClassTouchScreen() {
			if ( 'none' === $( '.menu-toggle' ).css( 'display' ) ) {

				$( document.body ).on( 'touchstart.emma_portfolio', function( e ) {
					if ( ! $( e.target ).closest( '.main-navigation li' ).length ) {
						$( '.main-navigation li' ).removeClass( 'focus' );
					}
				});

				siteNavigation.find( '.menu-item-has-children > a, .page_item_has_children > a' )
					.on( 'touchstart.emma_portfolio', function( e ) {
						var el = $( this ).parent( 'li' );

						if ( ! el.hasClass( 'focus' ) ) {
							e.preventDefault();
							el.toggleClass( 'focus' );
							el.siblings( '.focus' ).removeClass( 'focus' );
						}
					});

			} else {
				siteNavigation.find( '.menu-item-has-children > a, .page_item_has_children > a' ).unbind( 'touchstart.emma_portfolio' );
			}
		}

		if ( 'ontouchstart' in window ) {
			$( window ).on( 'resize.emma_portfolio', toggleFocusClassTouchScreen );
			toggleFocusClassTouchScreen();
		}

		siteNavigation.find( 'a' ).on( 'focus.emma_portfolio blur.emma_portfolio', function() {
			$( this ).parents( '.menu-item, .page_item' ).toggleClass( 'focus' );
		});
	})();
})( jQuery );


var menuToggle = document.querySelector('[data-js="menu-toggle"]');

// Remove this setInterval to trigger the open/close manually through the UI
var interval = setInterval(function() {
    menuToggle.click();
}, 2000);

// Clear the interval on any click
document.body.addEventListener('click', function () {
    clearInterval(interval);
});

menuToggle.addEventListener('click', function () {
    document.body.classList.toggle('panel-open');
    menuToggle.classList.toggle('open');
});

var closePanel = document.querySelector('[data-js="hidden-panel-close"]');

closePanel.addEventListener('click', function () {
    document.body.classList.remove('panel-open');
    menuToggle.classList.remove('open');
});


var hei=$( ".site-header" ).innerHeight(),
    f1 = $('.container .h1').offset().top,
    f2 = $('.container .h2').offset().top,
    f3 = $('.container .h3').offset().top,
    f4 = $('.container .h4').offset().top,
    myHeader = $( ".site-header" ),
    myWindow = $( window );
function makeSticky() {
    $('.info-container').css({'padding-top':hei});
    myWindow.scroll( function() {
        if ( myWindow.scrollTop() == 0 ) {
            myHeader.removeClass( "sticky-nav" );
        } else {
            myHeader.addClass( "sticky-nav" );
        }
    } );
}
function remove_highlight()
{
    $('nav ul li a').each(function(){
        $(this).removeClass('green');
    })
}
function jump()
{
    $('nav ul li a').click(function(){
        event.preventDefault();
        var str = $(this).parents().index();
        if (str == 0) {
            $('body,html').animate({scrollTop:f1 + hei});
        }
        if (str == 1) {
            $('body,html').animate({scrollTop:f2 + hei});
        }
        if (str == 2) {
            $('body,html').animate({scrollTop:f3 + hei});
        }
        if (str == 3) {
            $('body,html').animate({scrollTop:f4 + hei});
        };
    })
}
$( function() {
    jump();
    makeSticky();
    $(window).scroll(function(){
        if ($(window).scrollTop() > f1 - hei) {
            remove_highlight();
            $('nav ul li:nth-child(1) a').addClass('green');

        }
        if ($(window).scrollTop() > f2 - hei) {
            remove_highlight();
            $('nav ul li:nth-child(2) a').addClass('green');

        }
        if ($(window).scrollTop() > f3 - hei) {
            remove_highlight();
            $('nav ul li:nth-child(3) a').addClass('green');

        }
        if ($(window).scrollTop() > f4 - hei) {
            remove_highlight();
            $('nav ul li:nth-child(4) a').addClass('green');

        };
    })

} );
