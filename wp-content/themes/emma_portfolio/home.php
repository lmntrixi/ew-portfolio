<?php
if (have_rows('content_block')):
    while (have_rows('content_block')) :
        the_row();
        if (get_row_layout() == 'home_video_block'):
//video block
            ?>
            <div class="video-block">
                <div class="wrapper">
                    <div class="inner">
                        <iframe src="<?php the_sub_field('home_video_url'); ?>"
                                allowfullscreen="allowfullscreen"
                                mozallowfullscreen="mozallowfullscreen"
                                msallowfullscreen="msallowfullscreen"
                                oallowfullscreen="oallowfullscreen"
                                webkitallowfullscreen="webkitallowfullscreen">
                        </iframe>
                    </div>
                </div>
            </div>
        <?php
//uitgelicht project
        elseif (get_row_layout() == 'home_uitgelicht_project_block'):
            $images = get_sub_field('uitgelicht_fotos');
            ?>
            <div class="uitgelicht-project-block">
                <h2><?php the_sub_field('uitgelicht_title') ?></h2>
                <div class="inner">
                    <div class="row">
                        <div class="masonry-wrapper">
                            <?php if ($images): ?>
                                <?php foreach ($images as $image): ?>
                                    <div class="item grid-item grid-size">

                                        <img src="<?= $image['url'] ?>">
                                    </div>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="btn-wrap">
                        <a href="/index.php/portfolio" class="portfolio-btn">portfolio bekijken</a>
                    </div>
                </div>
            </div>
        <?php
//image en text
        elseif (get_row_layout() == 'home_txt_img_block'): ?>
            <section class="tekst-met-afbeelding-block block about">
                <div class="row">
                    <div class="image-side">
                        <div class="inner-wrap">
                            <div class="pattern"><img
                                        src="<?php the_sub_field('home_txt_img_patern') ?>">
                            </div>
                            <div class="image-wrap">
                                <img class="image-about rellax" data-rellax-speed="-1"
                                     src="<?php the_sub_field('home_txt_img_img') ?>">
                                <div class="image-background rellax" data-rellax-speed="1"
                                     style="background: <?php the_sub_field('home_txt_img_blok_kleur') ?>"></div>
                            </div>
                        </div>
                    </div>
                    <div class="text-side">
                        <div class="text-wrapper">
                            <h1 class="text-title"><?php the_sub_field('home_txt_img_title') ?></h1>
                            <h2 class="text-sub-title"><?php the_sub_field('home_txt_img_sub_title') ?></h2>
                            <div class="paragraph"><?php the_sub_field('home_txt_img_info') ?>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        <?php
//opvallende img
        elseif (get_row_layout() == 'opvallende_img_block'): ?>
            <div class="opvallende-visual-block">
                <div class="image-wrap">
                    <img src="<?php the_sub_field('opvallende_img_img') ?>" alt="visual">
                    <div class="text-wrap">
                        <h2 class="text-sub-title"><?php the_sub_field('opvallende_img_title') ?></h2>
                    </div>
                </div>
            </div>
        <?php
//text
        elseif (get_row_layout() == 'text_block'): ?>
            <div class="tekst-block">
                <div class="text-wrapper">
                    <h1 class="text-title"><?php the_sub_field('text_title') ?></h1>
                    <h2 class="text-sub-title"><?php the_sub_field('text_sub_title') ?></h2>
                    <div class="paragraph"><?php the_sub_field('text_info') ?>
                    </div>
                </div>
                                <div class="btn-wrap">
                                    <a href="#">optionele knop</a>
                                </div>
            </div>
        <?php
//diensten
        elseif (get_row_layout() == 'diensten_block'): ?>
            <div class="diensten-block">
                <div class="inner">
                    <h1 class="text-title"><?php the_sub_field('diensten_top_titel') ?></h1>
                    <div class="row">
                        <?php
                        while (have_rows('diensten')) : the_row(); ?>
                            <div class="item">
                                <div class="overflow">
                                    <a href="/index.php/portfolio">
                                        <div class="background-image"
                                             style="background-image: url('<?php the_sub_field('diensten_img') ?>')">
                                        </div>
                                    </a>
                                </div>
                                <div class="title"><?php the_sub_field('diensten_titel') ?></div>
                                <div class="info"><?php the_sub_field('diensten_info') ?></div>
                            </div>
                        <?php endwhile; ?>
                    </div>
                </div>
            </div>
        <?php
//tutorials
        elseif (get_row_layout() == 'tutorials'): ?>
            <div class="video-highlight-block">
                <div class="align">
                    <h1 class="text-title">Tutorials extra lange titel</h1>
                    <div class="inner">
                        <div class="row">
                            <?php
                            while (have_rows('tutorials')) : the_row(); ?>
                                <iframe src="<?php the_sub_field('tutorial_url') ?>"
                                        class="item"
                                        allowfullscreen="allowfullscreen"
                                        mozallowfullscreen="mozallowfullscreen"
                                        msallowfullscreen="msallowfullscreen"
                                        oallowfullscreen="oallowfullscreen"
                                        webkitallowfullscreen="webkitallowfullscreen" frameborder="0">
                                </iframe>
                            <?php endwhile; ?>
                            <div class="see-more item">
                                <a href="/index.php/tutorials">
                                    <div class="background-image"
                                         style="background-image: url('http://stijnolthof.nl/make-up.jpg')">
                                        <div class="text-wrap">
                                            <div class="text">Alle tutorials bekijken</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php
        endif;
    endwhile;
endif; ?>
