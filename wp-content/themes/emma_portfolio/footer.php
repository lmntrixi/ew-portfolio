</div><!-- #content -->

<footer id="colophon" class="site-footer" role="contentinfo" style="border-top: 2px solid black;">
    <div class="footer-wrapper">
        <div class="wrapper-inner">
            <div class="row">
                <div class="side-map">
                    <h2>SITEMAP</h2>
                    <ul>
                        <?php while (have_rows('nav', 'navigation')) : the_row(); ?>
                            <li>
                                <a href="<?php the_sub_field('nav_url') ?>"> <?php the_sub_field('nav_display_text', 'navigation'); ?></a>
                            </li>
                        <?php endwhile; ?>
                    </ul>
<!--                    <a href="#" class="contact-btn">Contact me</a>-->
                </div>


                <div class="contact">
                    <div class="wrap">
                        <h2>CONTACT</h2>
                        <?php while (have_rows('contact_mij', 'footer')) : the_row(); ?>
                            <p><?php the_sub_field('info') ?></p>
                        <?php endwhile; ?>
                    </div>
                </div>

                <div class="social">
                    <div class="wrap">
                        <h2>SOCIAL</h2>
                        <div class="image-wrap">
                            <span><img src="http://emma.stijnolthof.nl/wp-content/uploads/2018/06/instagram-1.png"></span>
                            <span><img src="http://emma.stijnolthof.nl/wp-content/uploads/2018/06/instagram-2.png"></span>
                            <span><img src="http://emma.stijnolthof.nl/wp-content/uploads/2018/06/instagram-3.png"></span>
                            <span><img src="http://emma.stijnolthof.nl/wp-content/uploads/2018/06/instagram-4.png"></span>
                            <span><img src="http://emma.stijnolthof.nl/wp-content/uploads/2018/06/instagram-5.png"></span>
                            <span><img src="http://emma.stijnolthof.nl/wp-content/uploads/2018/06/instagram-6.png"></span>
                        </div>
                        <div class="socials">
                            <a href="<?php the_field('facebook_link', 'footer') ?>" class="social-icons facebook"><span class="icon-facebook"></span></a>
                            <a href="<?php the_field('twitter_link', 'footer') ?>" class="social-icons twitter"><span class="icon-twitter"></span></a>
                            <a href="<?php the_field('youtube_link', 'footer') ?>" class="social-icons insta"><span class="youtube"></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <p><?php the_field('nav', 'footer'); ?></p>

</footer><!-- #colophon -->
</div><!-- .site-content-contain -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
