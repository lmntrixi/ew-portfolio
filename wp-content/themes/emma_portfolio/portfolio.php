<?php
if (have_rows('portfolio')) { ?>
    <div class="info-popup-wrapper"></div>
    <div class="container">
        <div class="filter-wrapper">
            <ul class="mobile-filter">
                <li>Filter op</li>
            </ul>
            <div class="filter-items">
                <label class="btn btn-default active-click">
                    <input type="checkbox" value="all">
                    All
                </label>
                <label class="btn btn-default active-click">
                    <input type="checkbox" value="bruiloft">
                    Bruiloft
                </label>
                <label class="btn btn-default active-click">
                    <input type="checkbox" value="sfx">
                    SFX
                </label>
                <label class="btn btn-default active-click">
                    <input type="checkbox" value="high_fashion">
                    High fashion
                </label>
                <label class="btn btn-default active-click">
                    <input type="checkbox" value="visagie">
                    Visagie
                </label>
                <label class="btn btn-default active-click">
                    <input type="checkbox" value="hairstyling">
                    Hairstyling
                </label>
            </div>
        </div>
        <div class="custom-row">
            <div class="masonry-wrapper">
                <?php while (have_rows('portfolio')) : the_row(); ?>
                    <?php
                    $image = get_sub_field('portfolio_main_img');
                    $images = get_sub_field('portfolio_detail_img');

                    $size = 'full';

                    $tags = get_sub_field('portfolio_tags');
                    $tags = str_replace(",", "", $tags);
                    $tags = implode(' ', $tags);
                    $tags = strtolower($tags);
                    ?>


                    <div class="portfolio-image  grid-item grid-size all <?= $tags; ?>">
                        <div class="image-slice item">
                            <img src="<?= $image ?>">
                        </div>
                        <div class="info-wrapper">
                            <div class="inner row">
                                <div class="close-icon"></div>
                                <div class="inner-scroll">
                                    <div class="image-wrap">
                                        <div class="mason-wrap">
                                            <div class="masonry-wrapper2">
                                                <div class="item grid-item grid-size">
                                                    <img src="<?= $image ?>">
                                                </div>

                                                <?php if ($images): ?>
                                                    <?php foreach ($images as $image): ?>
                                                        <div class="item grid-item grid-size">
                                                            <img src="<?= $image['url'] ?>">
                                                        </div>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="info-block">
                                        <div class="inner">

                                            <p class="image-title"><?php the_sub_field('portfolio_title'); ?></p>
                                            <p class="sub-title"><?php the_sub_field('portfolio_sub_title') ?>
                                                - <?php the_sub_field('portfolio_datum') ?></p>
                                            <div class="info-wraps">
                                                <?php the_sub_field('portfolio_info'); ?>
                                            </div>
                                            <a href="/index.php/contact">
                                                <div class="contact-btn" style="margin-top: 25px">contact</div>
                                            </a>
                                            <?php if (get_sub_field('portfolio_react') == 'Ja') : ?>
                                                <a href="/index.php/tutorials">
                                                    <div class="contact-btn">recreate this look</div>
                                                </a>
                                            <?php endif; ?>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                <?php endwhile; ?>
            </div>
        </div>
    </div>
<?php } ?>

<script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>

<script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.js"></script>
<script>
    (function ($) {
        'use strict';
        $.fn.multipleFilterMasonry = function (options) {
            var cache = [];
            var filters = [];

            if (options.selectorType === 'list') {
                $(options.filtersGroupSelector).children().each(function () {
                    filters.push($(this).data('filter'));
                });
            }

            //the main job of the function is to cache the item,because we are going to filter the items later
            var init = function ($container) {
                $container.find(options.itemSelector).each(function () {
                    cache.push($(this));
                });
                $container.masonry(options);
            };

            //filter items in cache
            var filterItems = function (selector) {
                var result = [];
                $(cache).each(function (item) {
                    $(selector).each(function (index, sel) {
                        if (cache[item].is(sel)) {
                            if ($.inArray(cache[item], result) === -1) result.push(cache[item]);
                        }
                    });
                });
                return result;
            };

            //reload masonry
            var reload = function ($container, items) {
                $container.empty();
                $(items).each(function () {
                    $($container).append($(this));
                });
                $container.masonry('reloadItems');
                $container.masonry();
            };


	        $('.masonry-wrapper').on('click', '.item', function () {
		        setTimeout(function(){
			        $container2.masonry();
                }, 200);
	        });


            $(document).ready(function () {
                $container.masonry();
            });


            $(window).resize(function() {
                setTimeout(function () {
                    console.log('trigger');
                    $container2.masonry();
                }, 1000);
            });



            // Hash filter
            var hashFilter = function ($container) {
                var hash = window.location.hash.replace("#", "");
                if ($.inArray(hash, filters) !== -1) {
                    reload($container, $('.' + hash));
                }
            };

            var proc = function ($container) {
                $(options.filtersGroupSelector).find('input[type=checkbox]').each(function () {
                    $(this).change(function () {
                        var selector = [];
                        $(options.filtersGroupSelector).find('input[type=checkbox]').each(function () {
                            if ($(this).is(':checked')) {
                                selector.push('.' + $(this).val());
                            }
                        });
                        var items = cache;
                        if (selector.length > 0) {
                            items = filterItems(selector);
                        }
                        reload($container, items);///
                    });
                });
            };

            var procUL = function ($container) {
                $(options.filtersGroupSelector).children().each(function () {
                    $(this).click(function () {
                        $(options.filtersGroupSelector).children().removeClass('selected');
                        window.location.hash = $(this).data('filter');
                        var selector = [];
                        selector.push('.' + $(this).data('filter'));
                        $(this).addClass('selected');
                        var items = cache;
                        if (selector.length > 0) {
                            items = filterItems(selector);
                        }
                        reload($container, items);
                    });
                });

                hashFilter($container);
                $(options.filtersGroupSelector).children().removeClass('selected');
                $('.filters li[data-filter=' + window.location.hash.replace("#", "") + ']').addClass('selected');
            };

            return this.each(function () {
                var $$ = $(this);
                init($$);
                options.selectorType === 'list' ? procUL($$) : proc($$);
            });
        };
    }(window.jQuery));

</script>
<script>
    var $container = $('.masonry-wrapper');

    $('.masonry-wrapper').masonry({
        // options...
        columnWidth: '.grid-size',
        itemSelector: '.grid-item',
        percentPosition: true,
        horizontalOrder: true,
        filtersGroupSelector: '.filter'
        // console.log('1');
    });
    $container.multipleFilterMasonry({
        itemSelector: '.portfolio-image',
        filtersGroupSelector: '.filter-items'
    });

    var $container2 = $('.masonry-wrapper2');

    $('.masonry-wrapper2').masonry({
        // options...
        columnWidth: '.grid-size',
        itemSelector: '.grid-item',
        percentPosition: true,
        horizontalOrder: true,
        filtersGroupSelector: '.filter'
        // console.log('1');
    });
    $container2.multipleFilterMasonry({
        itemSelector: '.portfolio-image',
        filtersGroupSelector: '.filter-items'
    });
</script>


<script src="javascripts/masonry.pkgd.js"></script>
<script src="javascripts/multipleFilterMasonry.js"></script>
