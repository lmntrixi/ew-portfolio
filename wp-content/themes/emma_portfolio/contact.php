
<?php
if (get_field('contact_kop_text')) {

$image = get_field('img_contact');
$size = 'full';
?>

<section class="block-contact" style="padding-top: 130px">
    <div class="row">
        <div class="image-side">
            <div class="inner-wrap">
                <div class="pattern"><img src="<?php the_field('patroon_contact') ?>"></div>
                <div class="image-wrap">
                    <img class="image-about rellax" data-rellax-speed="-1" src="<?php echo $image; ?>">
                    <div class="image-background rellax" data-rellax-speed="1"></div>
                </div>
            </div>
        </div>
        <div class="text-side">
            <div class="text-wrapper">



                <h1 class="text-title"><?php the_field('contact_kop_text') ?></h1>
                <?php
                    $form = get_field('form_contact');
                    echo do_shortcode($form);
                ?>

            </div>
        </div>
    </div>
</section>

<?php } ?>
