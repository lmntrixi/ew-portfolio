<?php

get_header(); ?>

<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<section class="error-404 not-found">
                <div class="container-wrap">
                    <div class="row">
                        <div class="image">
                            <img src="https://drive.google.com/file/d/1oDaQIpomX1R1yrZxDbNok-8zWMmkn0mz/view">
                        </div>
                        <div class="text">
                            <h1><?php _e( 'Sorry deze pagina is niet gevonden' ); ?></h1>
                            <p><?php _e( 'We hebben ons best gedaan, maar het lijkt erop dat deze pagina niet (meer) bestaat of misschien verhuisd is.', 'emma_portfolio' ); ?></p>
                            <div class="btn-wrap">
                                <a href="/index.php/" class="portfolio-btn">Ga terug naar home</a>
                            </div>
                        </div>
                    </div>
                </div>

                <?php //_e( 'Oops! That page can&rsquo;t be found.', 'emma_portfolio' ); ?><!--</h1>-->
                <?php //_e( 'It looks like nothing was found at this location. Maybe try a search?', 'emma_portfolio' ); ?><!--</p>-->
                <?php ////get_search_form(); ?>

			</section>
		</main>
	</div>
</div>

<?php get_footer();
