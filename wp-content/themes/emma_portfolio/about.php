<?php
if (have_rows('content_areas')) {


    while (have_rows('content_areas')) : the_row(); ?>
        <?php
        $image = get_sub_field('img_over_mij');
        $size = 'full';
        ?>
        <section class="block about">
            <div class="row">
                <div class="quote"><p style=" color: <?php the_sub_field('colorPickerSpreuk_over_mij') ?>"><?php the_sub_field('spreuk_over_mij'); ?></p></div>
                <div class="image-side">
                    <div class="inner-wrap">
                        <div class="pattern"><img src="<?php the_sub_field('patroon_over_mij') ?>"></div>
                        <div class="image-wrap">
                            <img class="image-about rellax" data-rellax-speed="-1" src="<?php echo $image; ?>">
                            <div class="image-background rellax" data-rellax-speed="1" style="background: <?php the_sub_field('colorPickerBlock_over_mij') ?>"></div>
                        </div>
                    </div>
                </div>
                <div class="text-side">
                    <div class="text-wrapper">
                        <h1 class="text-title"><?php the_sub_field('title_over_mij'); ?></h1>
                        <div class="paragraph"><?php the_sub_field('text_over_mij'); ?></div>
                    </div>
                </div>
            </div>
        </section>
    <?php endwhile;
} ?>
